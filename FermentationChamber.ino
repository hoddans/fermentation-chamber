#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <EEPROMEx.h>
#include <Adafruit_MCP23017.h>
#include <Adafruit_RGBLCDShield.h>

// LCD Colors
#define OFF 0x0
#define RED 0x1
#define GREEN 0x2
#define YELLOW 0x3
#define BLUE 0x4
#define PURPLE 0x5
#define TEAL 0x6
#define WHITE 0x7

// Pins
#define pinFan 4
#define pinRelayCold 7
#define pinRelayHot 12
#define pinTempBus 8

// Config
#define configAddress 0
#define configVer 104

struct Configuration {
  int ver; // for compatibility
  // Temps
  float target;
  float variance;
  float outDiff;
  // Timing
  int coolMin;
  int coolMax;
  int coolOff;
  int hotMin;
  int hotMax;
  int hotOff;
  int fanOn;
  int fanOff;
  int backlight;
};
Configuration cfgSettings;

struct TimeMode {
  boolean on;
  long time;
};
TimeMode fridge;
TimeMode lamp;
TimeMode fan;
TimeMode backlight;

DeviceAddress thermIn = { 0x28, 0xDB, 0x35, 0x05, 0x04, 0x00, 0x00, 0x6B };
DeviceAddress thermOut = { 0x28, 0x44, 0x3F, 0x05, 0x04, 0x00, 0x00, 0x43 };
OneWire oneWire(pinTempBus);
DallasTemperature sensors(&oneWire);

Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();

float tempInt, tempExt;
int inMenu, refresh;
boolean goodFlag;

void setup() {
  pinMode(pinFan, OUTPUT);
  digitalWrite(pinFan, LOW);
  pinMode(pinRelayCold, OUTPUT);
  digitalWrite(pinRelayCold, LOW);
  pinMode(pinRelayHot, OUTPUT);
  digitalWrite(pinRelayHot, LOW);
  lcd.begin(16,2);
  lcd.setBacklight(WHITE);
  backlight.on = true;
  backlight.time = millis();
  lcd.setCursor(0,0);
  lcd.print("INITIALIZING....");
  lcd.setCursor(0,1);
  lcd.print("    SETTINGS    ");
  configLoad();
  lcd.setCursor(0,1);
  lcd.print("  TEMP SENSORS  ");
  sensors.begin();
  sensors.setResolution(thermIn, 12);
  sensors.setResolution(thermOut, 12);
  tempInt = 0.0;
  tempExt = 0.0;
  if (sensors.requestTemperaturesByAddress(thermIn)) tempInt = sensors.getTempF(thermIn);
  if (sensors.requestTemperaturesByAddress(thermOut)) tempExt = sensors.getTempF(thermOut);
  
  fridge.on = false;
  fridge.time = 0;
  lamp.on = false;
  lamp.time = 0;
  fan.on = false;
  fan.time = 0;
  
  inMenu = 0;
  refresh = 0;
  goodFlag = false;
}

void loop() {
  uint8_t button = lcd.readButtons();
  int tmpdelay = 0;
  
  if (millis() < fridge.time) fridge.time = millis(); // Don't screw up times during a millis reset
  if (millis() < lamp.time) lamp.time = millis();
  if (millis() < fan.time) fan.time = millis();
  
  if (button)
  {
    lcd.setBacklight(WHITE);
    backlight.on = true;
    backlight.time = millis();
    
    if ((button & BUTTON_UP) && inMenu > 0) changeSettings(1);
    if ((button & BUTTON_DOWN) && inMenu > 0) changeSettings(-1);
    if ((button & BUTTON_LEFT) && inMenu > 0)
    {
      inMenu -= 1;
      if (inMenu < 1) inMenu = 1;
      tmpdelay += 100;
    }
    if ((button & BUTTON_RIGHT) && inMenu > 0)
    {
      inMenu += 1;
      if (inMenu > 12) inMenu = 12;
      tmpdelay += 100;
    }
    if (button & BUTTON_SELECT)
    {
      if (inMenu > 0)
      {
        inMenu = 0;
        lcd.setCursor(0,0);
        lcd.print("     SAVING     ");
        lcd.setCursor(0,1);
        lcd.print("    SETTINGS    ");
        configSave();
      }
      else
      {
        inMenu = 1;
        tmpdelay += 250;
      }
    }
  }
  
  if (inMenu > 0)
  {
    drawMenu();
    if ((millis()-backlight.time)/1000 > 180) inMenu = 0;
  }
  else
  {
    refresh += 1;
    if (refresh == 2 || refresh == 4 || refresh == 6 || refresh == 8) // Keeps the buttons more responsive
    {
      if (sensors.requestTemperaturesByAddress(thermIn)) tempInt = sensors.getTempF(thermIn); // Fetch internal temp
    }
    if (refresh == 8) // Delay external temp
    {
      if (sensors.requestTemperaturesByAddress(thermOut)) tempExt = sensors.getTempF(thermOut); // Fetch external temp
      refresh = 0;
    }
    
    //  0123456789123456
    //0 Int:##.# Ex:##.#
    //1 Tar ##.# +/- #.#
    
    lcd.setCursor(0,0);
    lcd.print("Int:");
    lcd.print(tempInt);
    lcd.setCursor(8,0);
    lcd.print(" Ex:");
    lcd.print(tempExt);
    lcd.setCursor(0,1);
    lcd.print("Tar:");
    lcd.print(cfgSettings.target);
    lcd.setCursor(8,1);
    lcd.print(" +/- ");
    lcd.print(cfgSettings.variance);
  }
  
  if (tempInt > cfgSettings.target+cfgSettings.variance) // too hot inside
  {
    if ((!fridge.on && (millis()-fridge.time)/1000 > cfgSettings.coolOff && !(tempInt < cfgSettings.target+(cfgSettings.variance*2) && tempExt < cfgSettings.target-cfgSettings.outDiff)) || fridge.time == 0) // fridge off long enough
    {
      digitalWrite(pinRelayCold,HIGH);
      fridge.on = true;
      fridge.time = millis();
    }
  }
  else if (tempInt <= cfgSettings.target+.1) // turn off the fridge
  {
    if (fridge.on && (millis()-fridge.time)/1000 > cfgSettings.coolMin) // fridge on long enough
    {
      digitalWrite(pinRelayCold,LOW);
      fridge.on = false;
      fridge.time = millis();
    }
  }
  if (fridge.on && (millis()-fridge.time)/1000 > cfgSettings.coolMax) // fridge on too long
  {
    digitalWrite(pinRelayCold,LOW);
    fridge.on = false;
    fridge.time = millis();
  }
  
  if (tempInt < cfgSettings.target-cfgSettings.variance) // too cold inside
  {
    if ((!lamp.on && (millis()-lamp.time)/1000 > cfgSettings.hotOff && !(tempInt > cfgSettings.target-(cfgSettings.variance*2) && tempExt > cfgSettings.target+cfgSettings.outDiff)) || lamp.time == 0) // lamp off long enough
    {
      digitalWrite(pinRelayHot,HIGH);
      lamp.on = true;
      lamp.time = millis();
    }
  }
  else if (tempInt >= cfgSettings.target-.1) // turn off the lamp
  {
    if (lamp.on && (millis()-lamp.time)/1000 > cfgSettings.hotMin) // lamp on long enough
    {
      digitalWrite(pinRelayHot,LOW);
      lamp.on = false;
      lamp.time = millis();
    }
  }
  if (lamp.on && (millis()-lamp.time)/1000 > cfgSettings.hotMax) // lamp on too long
  {
    digitalWrite(pinRelayHot,LOW);
    lamp.on = false;
    lamp.time = millis();
  }
  if (!lamp.on && tempInt < cfgSettings.target && fridge.on && cfgSettings.coolMin-(millis()-fridge.time)/1000 >= 30 && (millis()-lamp.time)/1000 > cfgSettings.hotOff/2) // Turn on the lamp to counter the overage of the fridge
  {
    digitalWrite(pinRelayHot,HIGH);
    lamp.on = true;
    lamp.time = millis();
  }

  if (fan.on && (millis()-fan.time)/1000 > cfgSettings.fanOn && cfgSettings.fanOff > 0)
  {
    digitalWrite(pinFan,LOW);
    fan.on = false;
    fan.time = millis();
  }
  else if ((!fan.on && (millis()-fan.time)/1000 > cfgSettings.fanOff) || fan.time == 0)
  {
    digitalWrite(pinFan,HIGH);
    fan.on = true;
    fan.time = millis();
  }
  
  if (tempInt >= cfgSettings.target-cfgSettings.variance && tempInt <= cfgSettings.target+cfgSettings.variance)
  {
    if (!fridge.on && !lamp.on && !goodFlag)
    {
      lcd.setBacklight(GREEN);
      backlight.on = true;
      backlight.time = millis();
      goodFlag = true;
    }
  }
  else goodFlag = false;
  
  if ((refresh == 1 || refresh == 5) && inMenu == 0) // Set backlights every 4 loops
  {
    if (fridge.on || lamp.on)
    {
      if (fridge.on && !lamp.on) lcd.setBacklight(BLUE);
      else if (!fridge.on && lamp.on) lcd.setBacklight(RED);
      else lcd.setBacklight(PURPLE);
    }
    else
    {
      if (!backlight.on) lcd.setBacklight(OFF);
    }
  }
  
  if (backlight.on && (millis()-backlight.time)/1000 > cfgSettings.backlight && inMenu == 0 && !fridge.on && !lamp.on)
  {
    backlight.on = false;
    lcd.setBacklight(OFF);
  }
  
  delay(tmpdelay);
}

void changeSettings(int dir) {
  switch (inMenu) {
    case 1:
      cfgSettings.target += .1 * dir;
      if (cfgSettings.target < 30 || cfgSettings.target > 85) cfgSettings.target -= .1 * dir;
      break;
    case 2:
      cfgSettings.variance += .1 * dir;
      if (cfgSettings.variance < .1 || cfgSettings.variance > 5) cfgSettings.variance -= .1 * dir;
      break;
    case 3:
      cfgSettings.outDiff += .5 * dir;
      if (cfgSettings.outDiff < 5 || cfgSettings.outDiff > 99) cfgSettings.outDiff -= .5 * dir;
      break;
    case 4:
      cfgSettings.coolMin += 5 * dir;
      if (cfgSettings.coolMin < 120 || cfgSettings.coolMin > cfgSettings.coolMax ) cfgSettings.coolMin -= 5 * dir;
      break;
    case 5:
      cfgSettings.coolMax += 5 * dir;
      if (cfgSettings.coolMax < cfgSettings.coolMin || cfgSettings.coolMax > 1800) cfgSettings.coolMax -= 5 * dir;
      break;
    case 6:
      cfgSettings.coolOff += 5 * dir;
      if (cfgSettings.coolOff < 420 || cfgSettings.coolOff > 1800) cfgSettings.coolOff -= 5 * dir;
      break;
    case 7:
      cfgSettings.hotMin += 1 * dir;
      if (cfgSettings.hotMin < 15 || cfgSettings.hotMin > cfgSettings.hotMax) cfgSettings.hotMin -= 1 * dir;
      break;
    case 8:
      cfgSettings.hotMax += 1 * dir;
      if (cfgSettings.hotMax < cfgSettings.hotMin || cfgSettings.hotMax > 240) cfgSettings.hotMax -= 1 * dir;
      break;
    case 9:
      cfgSettings.hotOff += 5 * dir;
      if (cfgSettings.hotOff < 120 || cfgSettings.hotOff > 900) cfgSettings.hotOff -= 5 * dir;
      break;
    case 10:
      cfgSettings.fanOn += 5 * dir;
      if (cfgSettings.fanOn < 60 || cfgSettings.fanOn > 1800) cfgSettings.fanOn -= 5 * dir;
      break;
    case 11:
      cfgSettings.fanOff += 5 * dir;
      if (cfgSettings.fanOff < 0 || cfgSettings.fanOff > 1800) cfgSettings.fanOff -= 5 * dir;
      break;
    case 12:
      cfgSettings.backlight += 1 * dir;
      if (cfgSettings.backlight < 0 || cfgSettings.backlight > 60) cfgSettings.backlight -= 1 * dir;
      break;
  }
}

void drawMenu() {
  switch (inMenu) {
    case 1:
      lcd.setCursor(0,0);
      lcd.print("  TARGET TEMP  >");
      lcd.setCursor(0,1);
      lcd.print("          ");
      lcd.print(cfgSettings.target);
      lcd.setCursor(14,1);
      lcd.print(" F");
      break;
    case 2:
      lcd.setCursor(0,0);
      lcd.print("< TMP VARIANCE >");
      lcd.setCursor(0,1);
      lcd.print("         +/- ");
      lcd.print(cfgSettings.variance);
      break;
    case 3:
      lcd.setCursor(0,0);
      lcd.print("< OUTSIDE DIFF >");
      lcd.setCursor(0,1);
      lcd.print("          ");
      if (cfgSettings.outDiff < 10) lcd.print(" ");
      lcd.print(cfgSettings.outDiff);
      lcd.setCursor(14,1);
      lcd.print(" F");
      break;
    case 4:
      lcd.setCursor(0,0);
      lcd.print("< MIN COOLTIME >");
      lcd.setCursor(0,1);
      secsToMins(cfgSettings.coolMin);
      break;
    case 5:
      lcd.setCursor(0,0);
      lcd.print("< MAX COOLTIME >");
      lcd.setCursor(0,1);
      secsToMins(cfgSettings.coolMax);
      break;
    case 6:
      lcd.setCursor(0,0);
      lcd.print("< COOLOFF TIME >");
      lcd.setCursor(0,1);
      secsToMins(cfgSettings.coolOff);
      break;
    case 7:
      lcd.setCursor(0,0);
      lcd.print("< MIN HOT TIME >");
      lcd.setCursor(0,1);
      secsToMins(cfgSettings.hotMin);
      break;
    case 8:
      lcd.setCursor(0,0);
      lcd.print("< MAX HOT TIME >");
      lcd.setCursor(0,1);
      secsToMins(cfgSettings.hotMax);
      break;
    case 9:
      lcd.setCursor(0,0);
      lcd.print("< HOT OFF TIME >");
      lcd.setCursor(0,1);
      secsToMins(cfgSettings.hotOff);
      break;
    case 10:
      lcd.setCursor(0,0);
      lcd.print("< FAN ON TIME  >");
      lcd.setCursor(0,1);
      secsToMins(cfgSettings.fanOn);
      break;
    case 11:
      lcd.setCursor(0,0);
      lcd.print("< FAN OFF TIME >");
      lcd.setCursor(0,1);
      secsToMins(cfgSettings.fanOff);
      break;
    case 12:
      lcd.setCursor(0,0);
      lcd.print("<   BACKLIGHT   ");
      lcd.setCursor(0,1);
      secsToMins(cfgSettings.backlight);
      break;
  }
}

void configLoad() {
  Configuration cfgTemp;
  EEPROM.readBlock(configAddress, cfgTemp);
  if (cfgTemp.ver == configVer)
  {
    cfgSettings = cfgTemp;
  }
  else // Load defaults if the version doesn't match
  {
    cfgSettings.ver = configVer;
    cfgSettings.target = 67.0;
    cfgSettings.variance = .5;
    cfgSettings.outDiff = 15.0;
    cfgSettings.coolMin = 270;
    cfgSettings.coolMax = 1200;
    cfgSettings.coolOff = 600;
    cfgSettings.hotMin = 15;
    cfgSettings.hotMax = 40;
    cfgSettings.hotOff = 360;
    cfgSettings.fanOn = 180;
    cfgSettings.fanOff = 120;
    cfgSettings.backlight = 15;
    configSave();
  }
}

void configSave() {
  Configuration cfgTemp;
  EEPROM.readBlock(configAddress, cfgTemp);
  if (cfgTemp.ver == configVer) EEPROM.updateBlock(configAddress, cfgSettings);
  else EEPROM.writeBlock(configAddress, cfgSettings);
}

void secsToMins(int secs) {
  int mins = int(secs/60);
  if (mins > 0)
  {
    lcd.print(mins);
    lcd.print(" mins ");
  }
  if (secs % 60 > 0 || (secs % 60 == 0 && mins == 0))
  {
    lcd.print(secs % 60);
    lcd.print(" secs          ");
  }
  else lcd.print("         ");
}
